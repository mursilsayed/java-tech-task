package com.rezdy.lunch.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

@Repository
public class RecipeRepository {

    @Autowired
    private EntityManager entityManager;


    private void sortRecipes(List<Recipe> recipes) {
        Comparator<Ingredient> ingredientComparator= Comparator.comparing((Ingredient i)->i.getBestBefore());

        recipes.sort((Recipe r1,Recipe r2)->{
             LocalDate minimumBestBeforeRecipe1 = r1.getIngredients().stream().min(ingredientComparator).get().getBestBefore();
             LocalDate minimumBestBeforeRecipe2 = r2.getIngredients().stream().min(ingredientComparator).get().getBestBefore();

             // comparing in reverse order so that the recipe that has the minimum date goes to the bottom
             return minimumBestBeforeRecipe2.compareTo(minimumBestBeforeRecipe1);
        });

    }

    public List<Recipe> getNonExpiredRecipesOnDate(LocalDate date) {
        // JPQL Query "select r from Recipe r where not exists (select r1 from Recipe r1 join r1.ingredients i where i.useBy < '2020-11-11' and r1.id=r.id)"

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Recipe> criteriaQuery = cb.createQuery(Recipe.class);
        Root<Recipe> root = criteriaQuery.from(Recipe.class);
        root.fetch(Recipe_.ingredients);

        Subquery<Recipe> subquery = criteriaQuery.subquery(Recipe.class);
        Root<Recipe> subQueryRoot = subquery.from(Recipe.class);
        Join<Recipe, Ingredient> ingredientItem =subQueryRoot.join(Recipe_.ingredients,JoinType.INNER);

        Predicate expiredIngredient  = cb.lessThan(ingredientItem.get(Ingredient_.useBy),date);
        Predicate matchRecipe = cb.equal(root.get(Recipe_.id),subQueryRoot.get(Recipe_.id));
        subquery.select(subQueryRoot).where(expiredIngredient,matchRecipe);

        TypedQuery<Recipe> typedQuery = entityManager.createQuery(criteriaQuery.select(root).where(cb.exists(subquery).not()).distinct(true));

        List<Recipe> result = typedQuery.getResultList();
        sortRecipes(result);
        return result;
    }
}
