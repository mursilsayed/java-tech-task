package com.rezdy.lunch.service;

import com.rezdy.lunch.model.Recipe;
import com.rezdy.lunch.model.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.List;

@Service
public class LunchService {

    @Autowired
    private RecipeRepository recipeRepository;

    public List<Recipe> getNonExpiredRecipesOnDate(LocalDate date) {
        return recipeRepository.getNonExpiredRecipesOnDate(date);
    }



}
