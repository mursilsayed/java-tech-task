package com.rezdy.lunch.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.List;

@SpringBootTest
class RecipeRepositoryTest {
    @Autowired
    private RecipeRepository recipeRepository;

    @Test
    void testRecipeRepository() {
        List<Recipe> result = recipeRepository.getNonExpiredRecipesOnDate(LocalDate.parse("2020-11-11"));
        Assertions.assertTrue(result.size()==3);
        // validating that the recipe with the closest best before date appears in the last
        Assertions.assertTrue(result.get(2).getId() ==2);


        result = recipeRepository.getNonExpiredRecipesOnDate(LocalDate.parse("1998-11-11"));
        Assertions.assertTrue(result.size()==5);
        // validating that the recipe with the closest best before date appears in the last
        Assertions.assertTrue(result.get(4).getId() ==2);


    }

}