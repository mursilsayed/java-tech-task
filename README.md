# Lunch Microservice

The service provides an endpoint that will determine, from a set of recipes, what I can have for lunch at a given date, based on my fridge ingredient's expiry date, so that I can quickly decide what I’ll be having to eat, and the ingredients required to prepare the meal.

## Prerequisites

* [Docker](https://docs.docker.com/get-docker/) & [Docker-Compose](https://docs.docker.com/compose/install/)
* [Java 11 Runtime](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)

### Run

1. Start database:

    ```
    docker-compose up -d
    ```
   
2. Add test data from  `sql/lunch-data.sql` to the database. Here's a helper script if you prefer:

    ```
    CONTAINER_ID=$(docker inspect --format="{{.Id}}" lunch-db)
    ```
    
    ```
    docker cp sql/lunch-data.sql $CONTAINER_ID:/lunch-data.sql
    ```
    
    ```
    docker exec $CONTAINER_ID /bin/sh -c 'mysql -u root -prezdytechtask lunch </lunch-data.sql'
    ```
    
3. Run Springboot LunchApplication



## Test
If the web application is running with default configuration, you can access the api by making a `POST` 
request via curl to the following url `http://localhost:9000/lunch` . 
You need to provide a date as post param. 

*Sample Request* 

```
curl \
  --header "Content-type: application/x-www-form-urlencoded" \
  --request POST \
  --data "date=2020-11-11" \
http://localhost:9000/lunch
```

*Response*
```
[
    {
        "id": 1,
        "title": "Fry-up",
        "ingredients": [
            {
                "id": 1,
                "title": "Bacon",
                "bestBefore": "2030-12-31",
                "useBy": "2030-01-01"
            },
            {
                "id": 2,
                "title": "Baked Beans",
                "bestBefore": "2030-12-31",
                "useBy": "2030-01-01"
            },
            {
                "id": 8,
                "title": "Eggs",
                "bestBefore": "2030-12-31",
                "useBy": "2030-01-01"
            },
            {
                "id": 14,
                "title": "Mushrooms",
                "bestBefore": "2030-12-31",
                "useBy": "2030-01-01"
            },
            {
                "id": 17,
                "title": "Sausage",
                "bestBefore": "2030-12-31",
                "useBy": "2030-01-01"
            },
            {
                "id": 4,
                "title": "Bread",
                "bestBefore": "2030-12-31",
                "useBy": "2030-01-01"
            }
        ]
    },
    {
        "id": 3,
        "title": "Hotdog",
        "ingredients": [
            {
                "id": 11,
                "title": "Ketchup",
                "bestBefore": "2030-12-31",
                "useBy": "2030-01-01"
            },
            {
                "id": 15,
                "title": "Mustard",
                "bestBefore": "2030-12-31",
                "useBy": "2030-01-01"
            },
            {
                "id": 17,
                "title": "Sausage",
                "bestBefore": "2030-12-31",
                "useBy": "2030-01-01"
            },
            {
                "id": 10,
                "title": "Hotdog Bun",
                "bestBefore": "2030-12-31",
                "useBy": "2030-01-01"
            }
        ]
    },
    {
        "id": 2,
        "title": "Ham and Cheese Toastie",
        "ingredients": [
            {
                "id": 5,
                "title": "Butter",
                "bestBefore": "2030-12-31",
                "useBy": "2030-01-01"
            },
            {
                "id": 9,
                "title": "Ham",
                "bestBefore": "2030-12-31",
                "useBy": "2030-01-01"
            },
            {
                "id": 6,
                "title": "Cheese",
                "bestBefore": "1999-01-01",
                "useBy": "2030-01-01"
            },
            {
                "id": 4,
                "title": "Bread",
                "bestBefore": "2030-12-31",
                "useBy": "2030-01-01"
            }
        ]
    }
]

```