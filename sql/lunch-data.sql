CREATE TABLE ingredient (
    TITLE VARCHAR(256) NOT NULL,
    BEST_BEFORE DATE DEFAULT NULL,
    USE_BY DATE DEFAULT NULL,
    PRIMARY KEY (TITLE)
);

INSERT INTO ingredient (TITLE, BEST_BEFORE, USE_BY) VALUES
    ('Ham','2030-12-31','2030-01-01'),
    ('Cheese','1999-01-01','2030-01-01'),
    ('Bread','2030-12-31','2030-01-01'),
    ('Butter','2030-12-31','2030-01-01'),
    ('Bacon','2030-12-31','2030-01-01'),
    ('Eggs','2030-12-31','2030-01-01'),
    ('Baked Beans','2030-12-31','2030-01-01'),
    ('Mushrooms','2030-12-31','2030-01-01'),
    ('Sausage','2030-12-31','2030-01-01'),
    ('Hotdog Bun','2030-12-31','2030-01-01'),
    ('Ketchup','2030-12-31','2030-01-01'),
    ('Mustard','2030-12-31','2030-01-01'),
    ('Lettuce','2030-12-31','2030-01-01'),
    ('Tomato','2030-12-31','2030-01-01'),
    ('Cucumber','2030-12-31','2030-01-01'),
    ('Beetroot','2030-12-31','2030-01-01'),
    ('Salad Dressing','2030-12-31','1999-01-01'),
    ('Spinach','2030-12-31','1999-01-01'),
    ('Milk','2030-12-31','1999-01-01');

CREATE TABLE recipe (
     TITLE VARCHAR(256) NOT NULL,
     PRIMARY KEY (TITLE)
);

INSERT INTO recipe (title) VALUES
    ('Ham and Cheese Toastie'),
    ('Fry-up'),
    ('Salad'),
    ('Hotdog'),
    ('Omelette')
;

CREATE TABLE recipe_ingredient (
    recipe VARCHAR(256) NOT NULL,
    ingredient VARCHAR(256) NOT NULL,
    PRIMARY KEY (recipe, ingredient),
    CONSTRAINT FK_recipe FOREIGN KEY (recipe) REFERENCES recipe (TITLE),
    CONSTRAINT FK_ingredient FOREIGN KEY (ingredient) REFERENCES ingredient (TITLE)
);

INSERT INTO recipe_ingredient (recipe, ingredient) VALUES
    ('Ham and Cheese Toastie','Ham'),
    ('Ham and Cheese Toastie','Cheese'),
    ('Ham and Cheese Toastie','Bread'),
    ('Ham and Cheese Toastie','Butter'),

    ('Fry-up','Bacon'),
    ('Fry-up','Eggs'),
    ('Fry-up','Baked Beans'),
    ('Fry-up','Mushrooms'),
    ('Fry-up','Sausage'),
    ('Fry-up','Bread'),

    ('Salad','Lettuce'),
    ('Salad','Tomato'),
    ('Salad','Cucumber'),
    ('Salad','Beetroot'),
    ('Salad','Salad Dressing'),

    ('Hotdog','Hotdog Bun'),
    ('Hotdog','Sausage'),
    ('Hotdog','Ketchup'),
    ('Hotdog','Mustard'),

    ('Omelette','Eggs'),
    ('Omelette','Mushrooms'),
    ('Omelette','Milk'),
    ('Omelette','Spinach')
;


/* Migration script for creating unique identifiers for each table*/
RENAME TABLE ingredient TO ingredient_old;
RENAME TABLE recipe TO recipe_old;
RENAME TABLE recipe_ingredient TO recipe_ingredient_old;


CREATE TABLE IF NOT EXISTS ingredient (
    ID          INT(11) AUTO_INCREMENT PRIMARY KEY,
    TITLE       VARCHAR(256) NOT NULL,
    BEST_BEFORE DATE         NULL,
    USE_BY      DATE         NULL,
    INDEX (TITLE)
);
INSERT INTO ingredient(TITLE, BEST_BEFORE, USE_BY)
SELECT TITLE, BEST_BEFORE, USE_BY
FROM ingredient_old;


CREATE TABLE IF NOT EXISTS recipe (
    ID    INT(11) AUTO_INCREMENT PRIMARY KEY,
    TITLE VARCHAR(256) NOT NULL,
    INDEX (TITLE)
);
INSERT INTO recipe(TITLE)
SELECT TITLE
FROM recipe_old;

# CREATE TABLE IF NOT EXISTS recipe_ingredient (
#     ID            INT(11) AUTO_INCREMENT PRIMARY KEY,
#     RECIPE_ID     INT,
#     INGREDIENT_ID INT,
#     CONSTRAINT FK_recipe
#         FOREIGN KEY (RECIPE_ID) REFERENCES lunch.recipe(ID) ON DELETE CASCADE,
#     CONSTRAINT FK_ingredient
#         FOREIGN KEY (INGREDIENT_ID) REFERENCES lunch.ingredient(ID) ON DELETE CASCADE
# );
CREATE TABLE recipe_ingredient (
    id            INT AUTO_INCREMENT,
    recipe_id     INT NOT NULL,
    ingredient_id INT NOT NULL,
    CONSTRAINT recipe_ingredient_id_pk
        PRIMARY KEY (id),
    CONSTRAINT recipe_ingredient_recipe_ID_fk
        FOREIGN KEY (recipe_id) REFERENCES recipe(ID)
            ON DELETE CASCADE,
    CONSTRAINT recipe_ingredient_ingredient_ID_fk
        FOREIGN KEY (ingredient_id) REFERENCES ingredient(ID)
            ON DELETE CASCADE
);


INSERT INTO recipe_ingredient(RECIPE_ID, INGREDIENT_ID)
SELECT r.ID, i.ID
FROM recipe AS r
         JOIN recipe_ingredient_old ri ON r.TITLE = ri.recipe
         JOIN ingredient i ON i.TITLE = ri.ingredient;

DROP TABLE recipe_ingredient_old, recipe_old, ingredient_old

